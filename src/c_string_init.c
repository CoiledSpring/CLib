/**
* @Author: Jules <archjules>
* @Date:   2016-08-03T16:45:53+02:00
* @Last modified by:   archjules
* @Last modified time: 2016-08-03T17:36:10+02:00
*/

#include <stdlib.h>
#include <clib/string.h>

void c_string_init(C_String * str) {
  str->elements = NULL;
  str->count = 0;
  str->size_elements = 1;
  str->size_memory = 0;
}
