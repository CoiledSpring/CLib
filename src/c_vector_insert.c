/**
* @Author: Jules <archjules>
* @Date:   2016-08-03T16:57:31+02:00
* @Last modified by:   archjules
* @Last modified time: 2016-08-03T17:11:07+02:00
*/

#include <stdlib.h>
#include <string.h>
#include <clib/vector.h>

void * c_vector_insert(C_Vector * vector, int n, void * element) {
  void * dest; int i;
  if (n < 0 || n >= vector->count) return NULL;

  // Check if we need to resize
  if ((vector->count + 1) * vector->size_elements > vector->size_memory) {
    c_vector_resize(vector, (vector->count + 1) * 2);
  }

  for (i = vector->count; i >= n; i--) {
    memcpy(vector->elements + (i + 1) * vector->size_elements, vector->elements + i * vector->size_elements, vector->size_elements);
  }

  vector->count++;
  dest = vector->elements + (n * vector->size_elements);
  memcpy(dest, element, vector->size_elements);
  return dest;
}
