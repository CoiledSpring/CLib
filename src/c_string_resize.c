/**
* @Author: Jules <archjules>
* @Date:   2016-08-03T16:53:52+02:00
* @Last modified by:   archjules
* @Last modified time: 2016-08-03T16:55:51+02:00
*/

#include <stdlib.h>
#include <clib/string.h>

void c_string_resize(C_String * str, int nb_chars) {
  str->size_memory = nb_chars;
  str->elements = realloc(str->elements, str->size_memory);

  if (nb_chars < str->count) {
    str->count = nb_chars;
  }
}
