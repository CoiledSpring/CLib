/**
* @Author: Jules <archjules>
* @Date:   2016-08-03T15:08:24+02:00
* @Last modified by:   archjules
* @Last modified time: 2016-08-03T15:11:43+02:00
*/

#include <stdlib.h>
#include <clib/vector.h>

void c_vector_resize(C_Vector * vector, int nb_elements) {
  vector->size_memory = nb_elements * vector->size_elements;
  vector->elements = realloc(vector->elements, vector->size_memory);

  if (nb_elements < vector->count) {
    vector->count = nb_elements;
  }
}
