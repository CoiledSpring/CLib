/**
* @Author: Jules <archjules>
* @Date:   2016-08-03T14:49:15+02:00
* @Last modified by:   archjules
* @Last modified time: 2016-08-03T15:07:33+02:00
*/
#include <stdlib.h>
#include <clib/vector.h>

void c_vector_init(C_Vector * vector, int size_elements) {
  vector->size_elements = size_elements;
  vector->size_memory   = 0;
  vector->count         = 0;
  vector->elements      = NULL;
}
