/**
* @Author: Jules <archjules>
* @Date:   2016-08-03T15:14:05+02:00
* @Last modified by:   archjules
* @Last modified time: 2016-08-03T15:58:01+02:00
*/

#include <stdlib.h>
#include <clib/vector.h>

void c_vector_free(C_Vector * vector) {
  free(vector->elements);
}
