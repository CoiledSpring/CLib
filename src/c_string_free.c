/**
* @Author: Jules <archjules>
* @Date:   2016-08-03T17:23:26+02:00
* @Last modified by:   archjules
* @Last modified time: 2016-08-03T17:24:12+02:00
*/

#include <stdlib.h>
#include <clib/string.h>

void c_string_free(C_String * str) {
  free(str->elements);
}
