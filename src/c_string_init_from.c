/**
* @Author: Jules <archjules>
* @Date:   2016-08-03T17:31:26+02:00
* @Last modified by:   archjules
* @Last modified time: 2016-08-03T17:38:22+02:00
*/
#include <string.h>
#include <clib/string.h>

void c_string_init_from(C_String * str, char * orig) {
  str->elements = strdup(orig);
  str->count    = strlen(orig) + 1;
  str->size_elements = 1;
  str->size_memory = str->count;
}
