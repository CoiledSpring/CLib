/**
* @Author: Jules <archjules>
* @Date:   2016-08-03T17:40:12+02:00
* @Last modified by:   archjules
* @Last modified time: 2016-08-03T17:40:47+02:00
*/

#include <clib/string.h>

char * c_string_get_whole(C_String * str) {
  return (char *)str->elements;
}
