/**
* @Author: Jules <archjules>
* @Date:   2016-08-03T15:31:56+02:00
* @Last modified by:   archjules
* @Last modified time: 2016-08-03T15:50:32+02:00
*/

#include <stdlib.h>
#include <clib/vector.h>

void * c_vector_add(C_Vector * vector, void * element) {
  // Check if we need to resize
  if ((vector->count + 1) * vector->size_elements > vector->size_memory) {
    c_vector_resize(vector, (vector->count + 1) * 2);
  }

  vector->count++;
  return c_vector_set(vector, vector->count - 1, element);
}
