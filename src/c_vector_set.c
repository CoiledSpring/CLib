/**
* @Author: Jules <archjules>
* @Date:   2016-08-03T15:19:24+02:00
* @Last modified by:   archjules
* @Last modified time: 2016-08-03T15:24:27+02:00
*/

#include <stdlib.h>
#include <string.h>
#include <clib/vector.h>

void * c_vector_set(C_Vector * vector, int n, void * element) {
  void * dest = NULL;

  if (n <= vector->count) {
    dest = vector->elements + (n * vector->size_elements);
    memcpy(dest, element, vector->size_elements);
    return dest;
  } else {
    return NULL;
  }
}
