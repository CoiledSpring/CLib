/**
* @Author: Jules <archjules>
* @Date:   2016-08-03T15:58:28+02:00
* @Last modified by:   archjules
* @Last modified time: 2016-08-03T16:08:21+02:00
*/

#include <string.h>
#include <clib/vector.h>

void c_vector_delete(C_Vector * vector, int n) {
  if (!(n < 0 || n > vector->count)) {
    memcpy(vector->elements + (n * vector->size_elements),
           vector->elements + ((n + 1) * vector->size_elements),
           (vector->count - n - 1) * vector->size_elements);
    vector->count--;
  }
}
