/**
* @Author: Jules <archjules>
* @Date:   2016-08-03T15:16:51+02:00
* @Last modified by:   archjules
* @Last modified time: 2016-08-03T15:24:20+02:00
*/

#include <stdlib.h>
#include <clib/vector.h>

void * c_vector_get(C_Vector * vector, int n) {
  if (n <= vector->count) {
    return vector->elements + (n * vector->size_elements);
  } else {
    return NULL;
  }
}
