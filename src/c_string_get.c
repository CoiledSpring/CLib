/**
* @Author: Jules <archjules>
* @Date:   2016-08-03T17:24:47+02:00
* @Last modified by:   archjules
* @Last modified time: 2016-08-03T17:46:59+02:00
*/

#include <clib/vector.h>
#include <clib/string.h>

char c_string_get(C_String * str, int n) {
  return *(char *)c_vector_get(str, n);
}
