/**
* @Author: Jules <archjules>
* @Date:   2016-08-03T13:15:03+02:00
* @Last modified by:   archjules
* @Last modified time: 2016-08-03T16:57:09+02:00
*/

#ifndef C_VECTOR_H
#define C_VECTOR_H

typedef struct {
  void * elements;
  int size_memory;
  int size_elements;
  int count;
} C_Vector;

// Internal functions
void c_vector_init(C_Vector * vector, int size_elements);
void c_vector_resize(C_Vector * vector, int nb_elements);
void c_vector_free(C_Vector * vector);

// Functions you will most probably use a lot
void * c_vector_get(C_Vector * vector, int n);
void * c_vector_set(C_Vector * vector, int n, void * element);
void * c_vector_add(C_Vector * vector, void * element);
void * c_vector_insert(C_Vector * vector, int n, void * element);
void   c_vector_delete(C_Vector * vector, int n);

#endif
