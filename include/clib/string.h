/**
* @Author: Jules <archjules>
* @Date:   2016-08-03T16:39:58+02:00
* @Last modified by:   archjules
* @Last modified time: 2016-08-03T17:41:15+02:00
*/

#ifndef C_STRING_H
#define C_STRING_H
#include "vector.h"

typedef C_Vector C_String;

void c_string_init(C_String * str);
void c_string_init_from(C_String * str, char * orig);
void c_string_resize(C_String * str, int nb_chars);
void c_string_free(C_String * str);

char   c_string_get(C_String * str, int n);
char * c_string_get_whole(C_String * str);
void   c_string_set(C_String * str, int n, char c);
void   c_string_add(C_String * str, char c);
void   c_string_delete(C_String * str, int n);

#endif /* end of include guard: C_STRING_H */
