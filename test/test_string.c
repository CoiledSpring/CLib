/**
* @Author: Jules <archjules>
* @Date:   2016-08-03T17:27:05+02:00
* @Last modified by:   archjules
* @Last modified time: 2016-08-03T17:46:12+02:00
*/
#include <string.h>
#include <stdio.h>
#include <clib.h>

int main() {
  C_String str;

  c_string_init_from(&str, "Hello World !");

  if (strcmp(c_string_get_whole(&str), "Hello World !")) {
    return 1;
  }

  if (c_string_get(&str, 6) != 'W') {
    return 2;
  }

  return 0;
}
