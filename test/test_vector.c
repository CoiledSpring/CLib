/**
* @Author: Jules <archjules>
* @Date:   2016-08-03T15:47:50+02:00
* @Last modified by:   archjules
* @Last modified time: 2016-08-03T17:22:07+02:00
*/
#include <stdio.h>
#include <string.h>
#include <string.h>
#include <clib.h>

int main() {
  C_Vector vector;
  c_vector_init(&vector, sizeof(char));
  c_vector_add(&vector, "H");

  if (*(char *)c_vector_get(&vector, 0) != 'H') {
    return 1;
  }

  c_vector_set(&vector, 0, "N");
  if (*(char *)c_vector_get(&vector, 0) != 'N') {
    return 2;
  }

  c_vector_add(&vector, "e");
  if (*(char *)c_vector_get(&vector, 1) != 'e') {
    return 3;
  }

  c_vector_add(&vector, "w");
  c_vector_add(&vector, "l");
  c_vector_add(&vector, "\0");
  c_vector_delete(&vector, 3);
  if(strcmp((char *)vector.elements, "New")) {
    return 4;
  };

  c_vector_insert(&vector, 1, "i");
  c_vector_set(&vector, 3, "t");
  if(strcmp((char *)vector.elements, "Niet")) {
    return 5;
  }
  
  c_vector_free(&vector);
  return 0;
}
