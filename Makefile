
# INCLUDE CONFIGURATION
include $(CURDIR)/Makefile.cfg

# DEDUCED VARIABLES
ALLOBJ = $(SRC:%=$(OBJDIR)/%.o)
ALLINC = $(INC:%=$(INCDIR)/%.h)
ALLTST = $(TST:%=$(TSTDIR)/bin/%)

# RULES
## Make everything (default)
all: lib$(NAME).a

sh3:
	@ PLATFORM=sh3eb-elf- make -B all

## Test everything
test: $(ALLTST)

$(OBJDIR):
	mkdir -p $@

$(TSTDIR)/bin:
	mkdir -p $@

## Make an object file out of a C source file
$(OBJDIR)/%.o: $(SRCDIR)/%.c $(ALLINC)
	$(CC) -c -o $@ $< $(CFLAGS)

## Execute a test
$(TSTDIR)/bin/%: $(TSTDIR)/%.c $(ALLINC) $(TSTDIR)/bin lib$(NAME).a
	@ $(CC) -o $@ $< $(CFLAGS) -L. -l$(NAME)
	@ $@ || (echo "$@ failed." && exit 1)
	@ echo "$@ suceeded \o/"

## Make the library
lib$(NAME).a: $(OBJDIR) $(ALLOBJ)
	$(AR) rc $@ $(ALLOBJ)
	$(RANLIB) $@

## Clean the object files
clean:
	$(RM) $(ALLOBJ)

## Clean the object files and the binary
dist-clean: clean
	$(RM) lib$(NAME).a

## Remake the project
re: fclean all

.PHONY: all clean fclean re
